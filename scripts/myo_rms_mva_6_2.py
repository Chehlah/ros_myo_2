#!/usr/bin/env python

'''
	Sampling emg array to rms,mva emg array

	Window Size of Sample about 1 10 20 30 40 50

	start: 07/08/2020
    *Note: Edit just topic name of pub
           and limit of window size

    Last:  28/08/2020
	1 25 50 100 150 200 250 300
'''

import rospy
import sys
import numpy as np
from ros_myo.msg import EmgArray
from std_msgs.msg import Float64
from std_msgs.msg import Float64MultiArray

class rmsemg(object):

	read_EMG   = EmgArray()
	stack_EMG  = []
	limit      = 300 

	rms_EMG    = Float64MultiArray()
	mva_EMG	   = Float64MultiArray()
		

	def __init__(self, limit = None):
		rospy.init_node('myo_rms_mva', anonymous=True)
		rospy.Subscriber("/myo_raw/myo_emg", EmgArray , self.cbEMGraw)
		self.pub_rms_01 = rospy.Publisher("/myo_rec/rms_01", Float64MultiArray , queue_size=10)
		self.pub_mva_01 = rospy.Publisher("/myo_rec/mva_01", Float64MultiArray , queue_size=10)
		self.pub_rms_02 = rospy.Publisher("/myo_rec/rms_25", Float64MultiArray , queue_size=10)
		self.pub_mva_02 = rospy.Publisher("/myo_rec/mva_25", Float64MultiArray , queue_size=10)
		self.pub_rms_03 = rospy.Publisher("/myo_rec/rms_50", Float64MultiArray , queue_size=10)
		self.pub_mva_03 = rospy.Publisher("/myo_rec/mva_50", Float64MultiArray , queue_size=10)
		self.pub_rms_04 = rospy.Publisher("/myo_rec/rms_100", Float64MultiArray , queue_size=10)
		self.pub_mva_04 = rospy.Publisher("/myo_rec/mva_100", Float64MultiArray , queue_size=10)
		self.pub_rms_05 = rospy.Publisher("/myo_rec/rms_150", Float64MultiArray , queue_size=10)
		self.pub_mva_05 = rospy.Publisher("/myo_rec/mva_150", Float64MultiArray , queue_size=10)
		self.pub_rms_06 = rospy.Publisher("/myo_rec/rms_200", Float64MultiArray , queue_size=10)
		self.pub_mva_06 = rospy.Publisher("/myo_rec/mva_200", Float64MultiArray , queue_size=10)
		self.pub_rms_07 = rospy.Publisher("/myo_rec/rms_250", Float64MultiArray , queue_size=10)
		self.pub_mva_07 = rospy.Publisher("/myo_rec/mva_250", Float64MultiArray , queue_size=10)
		self.pub_rms_08 = rospy.Publisher("/myo_rec/rms_300", Float64MultiArray , queue_size=10)
		self.pub_mva_08 = rospy.Publisher("/myo_rec/mva_300", Float64MultiArray , queue_size=10)
		
		self.rms_EMG.data = [0.0]*8
		self.mva_EMG.data = [0.0]*8
		self.str_ = ("start : limit {}".format(self.limit))
		rospy.loginfo(self.str_)
	
	def cbEMGraw(self,data):
	
		''' rms emg '''
		self.stack_EMG.append(data.data)

		''' rms emg '''
		if len(self.stack_EMG) > self.limit:
			self.pub_rms_01.publish(self.rms_multical(self.stack_EMG[299:self.limit]))
			self.pub_rms_02.publish(self.rms_multical(self.stack_EMG[275:self.limit]))
			self.pub_rms_03.publish(self.rms_multical(self.stack_EMG[250:self.limit]))
			self.pub_rms_04.publish(self.rms_multical(self.stack_EMG[200:self.limit]))
			self.pub_rms_05.publish(self.rms_multical(self.stack_EMG[150:self.limit]))
			self.pub_rms_06.publish(self.rms_multical(self.stack_EMG[100:self.limit]))
			self.pub_rms_07.publish(self.rms_multical(self.stack_EMG[ 50:self.limit]))
			self.pub_rms_08.publish(self.rms_multical(self.stack_EMG[  0:self.limit]))

			self.pub_mva_01.publish(self.mva_multical(  1,self.stack_EMG[299:self.limit]))
			self.pub_mva_02.publish(self.mva_multical( 25,self.stack_EMG[275:self.limit]))
			self.pub_mva_03.publish(self.mva_multical( 50,self.stack_EMG[250:self.limit]))
			self.pub_mva_04.publish(self.mva_multical(100,self.stack_EMG[200:self.limit]))
			self.pub_mva_05.publish(self.mva_multical(150,self.stack_EMG[150:self.limit]))
			self.pub_mva_06.publish(self.mva_multical(200,self.stack_EMG[100:self.limit]))
			self.pub_mva_07.publish(self.mva_multical(250,self.stack_EMG[ 50:self.limit]))
			self.pub_mva_08.publish(self.mva_multical(300,self.stack_EMG[  0:self.limit]))

			self.stack_EMG.pop(0)
		else:
			pass
	
	def rms_multical(self,dataSets):		
		stack = []
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			self.rms_EMG.data[nn] = np.sqrt(np.mean(np.array(stack[nn])**2))
		return self.rms_EMG

	def mva_multical(self,nw,dataSets):
		stack =[]
		weights = np.ones(nw) / nw
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			ans = np.convolve(stack[nn], weights, mode='valid')
			self.mva_EMG.data[nn] = ans[0]
		return self.mva_EMG


if __name__ == '__main__':
	try:
		run_rms = rmsemg(sys.argv[1] if len(sys.argv) >= 2 else None)
		rospy.spin()
	except rospy.ROSInterruptException:
		pass