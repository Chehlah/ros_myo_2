#!/usr/bin/env python

'''
	Sampling emg array to rms,mva emg array

	Window Size of Sample about 50 100 150 200 250 300

	start: 13/03/2020
'''

import rospy
import sys
import numpy as np
from ros_myo.msg import EmgArray
from std_msgs.msg import Float64
from std_msgs.msg import Float64MultiArray

class rmsemg(object):

	read_EMG   = EmgArray()
	stack_EMG  = []
	limit      = 300

	rms_EMG    = Float64MultiArray()
	mva_EMG	   = Float64MultiArray()
		

	def __init__(self, limit = None):
		rospy.init_node('myo_rms_mva', anonymous=True)
		rospy.Subscriber("/myo_raw/myo_emg", EmgArray , self.cbEMGraw)
		self.pub_rms_050 = rospy.Publisher("/myo_rec/rms_050", Float64MultiArray , queue_size=10)
		self.pub_mva_050 = rospy.Publisher("/myo_rec/mva_050", Float64MultiArray , queue_size=10)
		self.pub_rms_100 = rospy.Publisher("/myo_rec/rms_100", Float64MultiArray , queue_size=10)
		self.pub_mva_100 = rospy.Publisher("/myo_rec/mva_100", Float64MultiArray , queue_size=10)
		self.pub_rms_150 = rospy.Publisher("/myo_rec/rms_150", Float64MultiArray , queue_size=10)
		self.pub_mva_150 = rospy.Publisher("/myo_rec/mva_150", Float64MultiArray , queue_size=10)
		self.pub_rms_200 = rospy.Publisher("/myo_rec/rms_200", Float64MultiArray , queue_size=10)
		self.pub_mva_200 = rospy.Publisher("/myo_rec/mva_200", Float64MultiArray , queue_size=10)
		self.pub_rms_250 = rospy.Publisher("/myo_rec/rms_250", Float64MultiArray , queue_size=10)
		self.pub_mva_250 = rospy.Publisher("/myo_rec/mva_250", Float64MultiArray , queue_size=10)
		self.pub_rms_300 = rospy.Publisher("/myo_rec/rms_300", Float64MultiArray , queue_size=10)
		self.pub_mva_300 = rospy.Publisher("/myo_rec/mva_300", Float64MultiArray , queue_size=10)
		
		self.rms_EMG.data = [0.0]*8
		self.mva_EMG.data = [0.0]*8
		self.str_ = ("start : limit {}".format(self.limit))
		rospy.loginfo(self.str_)
	
	def cbEMGraw(self,data):
	
		''' rms emg '''
		self.stack_EMG.append(data.data)

		''' rms emg '''
		if len(self.stack_EMG) > self.limit:
			self.rms_multical_050(self.stack_EMG[250:self.limit])
			self.mva_multical_050(self.stack_EMG[250:self.limit])
			self.rms_multical_100(self.stack_EMG[200:self.limit])
			self.mva_multical_100(self.stack_EMG[200:self.limit])
			self.rms_multical_150(self.stack_EMG[150:self.limit])
			self.mva_multical_150(self.stack_EMG[150:self.limit])
			self.rms_multical_200(self.stack_EMG[100:self.limit])
			self.mva_multical_200(self.stack_EMG[100:self.limit])
			self.rms_multical_250(self.stack_EMG[50:self.limit])
			self.mva_multical_250(self.stack_EMG[50:self.limit])
			self.rms_multical_300(self.stack_EMG[0:self.limit])
			self.mva_multical_300(self.stack_EMG[0:self.limit])
			self.stack_EMG.pop(0)
		else:
			pass
		
	def rms_multical_050(self,dataSets):		
		stack = []
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			self.rms_EMG.data[nn] = np.sqrt(np.mean(np.array(stack[nn])**2))
		self.pub_rms_050.publish(self.rms_EMG)
	def rms_multical_100(self,dataSets):		
		stack = []
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			self.rms_EMG.data[nn] = np.sqrt(np.mean(np.array(stack[nn])**2))
		self.pub_rms_100.publish(self.rms_EMG)
	def rms_multical_150(self,dataSets):		
		stack = []
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			self.rms_EMG.data[nn] = np.sqrt(np.mean(np.array(stack[nn])**2))
		self.pub_rms_150.publish(self.rms_EMG)
	def rms_multical_200(self,dataSets):		
		stack = []
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			self.rms_EMG.data[nn] = np.sqrt(np.mean(np.array(stack[nn])**2))
		self.pub_rms_200.publish(self.rms_EMG)
	def rms_multical_250(self,dataSets):		
		stack = []
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			self.rms_EMG.data[nn] = np.sqrt(np.mean(np.array(stack[nn])**2))
		self.pub_rms_250.publish(self.rms_EMG)
	def rms_multical_300(self,dataSets):		
		stack = []
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			self.rms_EMG.data[nn] = np.sqrt(np.mean(np.array(stack[nn])**2))
		self.pub_rms_300.publish(self.rms_EMG)

	def mva_multical_050(self,dataSets):
		stack =[]
		weights = np.ones(50) / 50
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			ans = np.convolve(stack[nn], weights, mode='valid')
			self.mva_EMG.data[nn] = ans[0]
		self.pub_mva_050.publish(self.mva_EMG)
	def mva_multical_100(self,dataSets):
		stack =[]
		weights = np.ones(100) / 100
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			ans = np.convolve(stack[nn], weights, mode='valid')
			self.mva_EMG.data[nn] = ans[0]
		self.pub_mva_100.publish(self.mva_EMG)
	def mva_multical_150(self,dataSets):
		stack =[]
		weights = np.ones(150) / 150
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			ans = np.convolve(stack[nn], weights, mode='valid')
			self.mva_EMG.data[nn] = ans[0]
		self.pub_mva_150.publish(self.mva_EMG)
	def mva_multical_200(self,dataSets):
		stack =[]
		weights = np.ones(200) / 200
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			ans = np.convolve(stack[nn], weights, mode='valid')
			self.mva_EMG.data[nn] = ans[0]
		self.pub_mva_200.publish(self.mva_EMG)
	def mva_multical_250(self,dataSets):
		stack =[]
		weights = np.ones(250) / 250
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			ans = np.convolve(stack[nn], weights, mode='valid')
			self.mva_EMG.data[nn] = ans[0]
		self.pub_mva_250.publish(self.mva_EMG)
	def mva_multical_300(self,dataSets):
		stack =[]
		weights = np.ones(300) / 300
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			ans = np.convolve(stack[nn], weights, mode='valid')
			self.mva_EMG.data[nn] = ans[0]
		self.pub_mva_300.publish(self.mva_EMG)

if __name__ == '__main__':
	try:
		run_rms = rmsemg(sys.argv[1] if len(sys.argv) >= 2 else None)
		rospy.spin()
	except rospy.ROSInterruptException:
		pass