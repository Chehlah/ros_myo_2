#!/usr/bin/env python

'''
	Sampling emg array to rms emg array

	start: 29/01/2020
'''

import rospy
import sys
import numpy as np
from ros_myo.msg import EmgArray
from std_msgs.msg import Float64
from std_msgs.msg import Float64MultiArray

class rmsemg(object):

	read_EMG   = EmgArray()
	stack_EMG  = []
	rms_EMG    = Float64MultiArray()
	mva_EMG	   = Float64MultiArray()

	raw_in_0 = []
	rms_out_0 = Float64()

	limit = 300 # window size

	def __init__(self, limit = None):
		rospy.init_node('myo_rms', anonymous=True)
		rospy.Subscriber("/myo_raw/myo_emg", EmgArray , self.cbEMGraw)
		self.pub_rms_0 = rospy.Publisher("/myo_rms/rms_0", Float64 , queue_size=10)
		self.pub_rms_a = rospy.Publisher("/myo_rms/rms_list", Float64MultiArray , queue_size=10)
		self.pub_mva_a = rospy.Publisher("/myo_rms/mva_list", Float64MultiArray , queue_size=10)
		
		# if limit == None:
		# 	self.limit = 100
		# else:
		# 	self.limit = limit
		# print ("limit value = {}".format(self.limit))
		# self.rms_out_0.data = 0.0
		self.rms_EMG.data = [0.0]*8 
		self.mva_EMG.data = [0.0]*8
		self.str_ = ("start : limit {}".format(self.limit))
		rospy.loginfo(self.str_)
	
	def cbEMGraw(self,data):
		# self.read_EMG = data
		# str_ = ("EMG (1){:4d} (2){:4d} (3){:4d} (4){:4d} (5){:4d} (6){:4d} (7){:4d} (8){:4d}"
		# 	.format(self.read_EMG.data[0],self.read_EMG.data[1],self.read_EMG.data[2],self.read_EMG.data[3],
		# 	self.read_EMG.data[4],self.read_EMG.data[5],self.read_EMG.data[6],self.read_EMG.data[7]))
		# rospy.loginfo(str_)
		
		''' rms eng '''
		# self.raw_in_0.append(self.read_EMG.data[0])
		self.stack_EMG.append(data.data)

		# # RAW emg
		# self.rms_EMG.data = data.data
		# self.pub_rms_a.publish(self.rms_EMG)
		
		## Demo test 1 channel
		# if len(self.raw_in_0) > 50:
		# 	self.rms_cal(self.raw_in_0[0:50])
		# 	self.raw_in_0.pop(0)

		''' rms eng '''
		if len(self.stack_EMG) > self.limit:
			self.rms_multical(self.stack_EMG[0:self.limit])
			self.mva_multical(self.stack_EMG[0:self.limit])
			self.stack_EMG.pop(0)
		else:
			pass
		
	
	def rms_cal(self,data):
		y = np.array(data)
		rms = np.sqrt(np.mean(y**2))
		self.rms_out_0.data = rms
		self.pub_rms_0.publish(self.rms_out_0)

	def rms_multical(self,dataSets):
		# stack = []
		# rotate_stack = []
		# for nn in range(0,8):
		# 	rotate_stack.append([i[nn] for i in data50])
		# stack = np.array(rotate_stack)
		# for nn in range(0,8):
		# 	self.rms_EMG.data[nn] = np.sqrt(np.mean(stack[nn]**2))
		# self.pub_rms_a.publish(self.rms_EMG)
		
		stack = []
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			self.rms_EMG.data[nn] = np.sqrt(np.mean(np.array(stack[nn])**2))
		self.pub_rms_a.publish(self.rms_EMG)

	def mva_multical(self,dataSets):
		stack =[]
		weights = np.ones(self.limit) / self.limit
		for nn in range(0,8):
			stack.append([i[nn] for i in dataSets])
			ans = np.convolve(stack[nn], weights, mode='valid')
			self.mva_EMG.data[nn] = ans[0]
		self.pub_mva_a.publish(self.mva_EMG)

if __name__ == '__main__':
	try:
		run_rms = rmsemg(sys.argv[1] if len(sys.argv) >= 2 else None)
		rospy.spin()
	except rospy.ROSInterruptException:
		pass